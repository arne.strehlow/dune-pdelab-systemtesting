#dune_add_system_test(SOURCE navierstokesTaylorhood.cc
#                      BASENAME navierstokesTaylorhood
#                      INIFILE navierstokesTaylorhood.mini
#                      )

dune_add_system_test(SOURCE navierstokesTaylorhood.cc
                     BASENAME navierstokesTaylorhood_convergencetest
                     INIFILE navierstokesTaylorhood_convergencetest.mini
                     SCRIPT dune_convergencetest.py
                     )
