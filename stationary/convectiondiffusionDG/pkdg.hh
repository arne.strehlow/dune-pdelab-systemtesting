// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_PDELAB_FINITEELEMENTMAP_PkDG_HH
#define DUNE_PDELAB_FINITEELEMENTMAP_PkDG_HH


#include <dune/pdelab/finiteelementmap/opbfem.hh>
#include <dune/pdelab/finiteelementmap/finiteelementmap.hh>

namespace Dune {
  namespace PDELab {

    //! Switch between different basis for PkDGLocalFiniteElementMap
    enum class PkDGBasisPolynomial { lagrange, legendre, lobatto, l2orthonormal};

#ifndef DOXYGEN
    // Class declaration. Use template specialization below.
    template<class D, class R, int k, int d, PkDGBasisPolynomial p = PkDGBasisPolynomial::l2orthonormal>
    class PkDGLocalFiniteElementMap;
#endif


    /** \brief Pk discontinuous Galerkin FiniteElementMap based on
     * an L2 orthonormal polynomials
     *
     * If you have gmp the computation field for the l2 orthonormal
     * basis polynomials is Dune::GMPField<512>.
     *
     * \tparam D Type used for coordinates
     * \tparam R Type used for shape function values
     * \tparam k Order of polynomial basis
     * \tparam d Grid dimension
     *
     * \note Use OPBLocalFiniteElementMap directly if you need more
     * customization.
     *
     * \ingroup FiniteElementMap
     */
   template<class D, class R, int k, int d>
    class PkDGLocalFiniteElementMap<D,R,k,d,PkDGBasisPolynomial::l2orthonormal>
      : public OPBLocalFiniteElementMap<D,R,k,d,Dune::GeometryType::simplex,
#if HAVE_GMP
                                        Dune::GMPField<512>,
#else
                                        R,
#endif
                                        Dune::PB::BasisType::Pk>
    {
    public:

      //! return type of polynomial basis
      static constexpr PkDGBasisPolynomial polynomial()
      {
        return PkDGBasisPolynomial::l2orthonormal;
      }

      //! return order of polynomial basis
      static constexpr std::size_t order()
      {
        return k;
      }

    };

  }
}

#endif // DUNE_PDELAB_FINITEELEMENTMAP_PkDG_HH
