/********************************************************/
// Beware of line number changes, they may corrupt docu!
//! \brief Driver function to set up and solve the problem
/********************************************************/

#include <dune/pdelab/finiteelementmap/qkdg.hh>
#include "pkdg.hh"
#include <dune/pdelab/localoperator/convectiondiffusiondg.hh>
#include <dune/pdelab/constraints/noconstraints.hh>
#include "../parameterclasses/convectiondiffusionparametersamples.hh"


template<class GV>
int driver (const GV& gv, Dune::ParameterTree& ptree)
{
  // important types
  typedef typename GV::Grid::ctype DF; // type for ccordinates
  typedef double RF;                   // type for computations

  //Assemble Parameters
  using CDModel = MODELCLASS<GV,RF>;
  CDModel model;

  // dimension and important types
  const int dim = model.dimension();

  //To be done: Throw exception if dim not equal DIMENSION (from mini file)
  if (dim != DIMENSION) {
  std::cout << "Error: Dimensions of grid and equation do not match." << std::endl;
  return 1;
  }


  // Make grid function space. The name FEM is a copy&paste artifact from the finite element implementation.
  typedef FEMTYPE<DF,RF, DEGREE, DIMENSION> FEM;
  FEM fem;
  typedef Dune::PDELab::NoConstraints CON;
  typedef Dune::PDELab::ISTL::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem);
  gfs.name("P1");
  gfs.update();



  //Define boundary condition type.
  auto blambda = [&model](const auto& is, const auto& x){return model.bctype(is,x) > 0;};
  auto b = Dune::PDELab::
    makeBoundaryConditionFromCallable(gv,blambda);


  //Assemble constraints

  typedef typename GFS::template
    ConstraintsContainer<RF>::Type CC;
  CC cc;
  Dune::PDELab::constraints(b,gfs,cc); // assemble constraints
  std::cout << "constrained dofs=" << cc.size() << " of "
            << gfs.globalSize() << std::endl;

  // A coefficient vector
  using Z = Dune::PDELab::Backend::Vector<GFS,RF>;
  Z z(gfs); // initial value

  // Make a grid function out of it
  typedef Dune::PDELab::DiscreteGridFunction<GFS,Z> ZDGF;
  ZDGF zdgf(gfs,z);


  // Make Grid Function from Boundary Conditions
  auto glambda = [&](const auto& e, const auto& x){return model.g(e,x);};
  auto g = Dune::PDELab::makeGridFunctionFromCallable(gv,glambda);


  /*
  // Fill the coefficient vector with boundary condition
  Dune::PDELab::interpolate(g,gfs,z);
  //Delete boundary condition information from interior
  Dune::PDELab::set_nonconstrained_dofs(cc,0.0,z);
 */


  // Make a local operator
  typedef Dune::PDELab::ConvectionDiffusionDG<CDModel,FEM> LOP;
  auto methodname = ptree.get("method","0");
  Dune::PDELab::ConvectionDiffusionDGMethod::Type method;
  if (methodname == "SIPG") {
      method = Dune::PDELab::ConvectionDiffusionDGMethod::SIPG;
  }
  else if (methodname == "NIPG") {
      method = Dune::PDELab::ConvectionDiffusionDGMethod::NIPG;
  }
  else {
     std::cout << "Error: Expected method=NIPG or method=SIPG in ini file not found." << std::endl;
     return 1;
  }
  Dune::PDELab::ConvectionDiffusionDGWeights::Type weights = Dune::PDELab::ConvectionDiffusionDGWeights::weightsOn;
  LOP lop(model,method,weights,2.0);

   // Make a global operator
  typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
  MBE mbe((2*dim+1)*std::pow(DEGREE+1,dim)); // guess nonzeros per row
  typedef Dune::PDELab::GridOperator<
    GFS,GFS,  /* ansatz and test space */
    LOP,      /* local operator */
    MBE,      /* matrix backend */
    RF,RF,RF, /* domain, range, jacobian field type*/
    CC,CC     /* constraints for ansatz and test space */
    > GO;
  GO go(gfs,cc,gfs,cc,lop,mbe);

  // Select a linear solver backend
//   typedef Dune::PDELab::ISTLBackend_SEQ_UMFPack LS;
//   LS ls;

//   typedef Dune::PDELab::ISTLBackend_SEQ_CG_AMG_SSOR<GO> LS;
//   LS ls(50,2);

  typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_SSOR LS;
  LS ls (5000 , true ) ;


  // Assemble and solve linear problem
  typedef Dune::PDELab::
    StationaryLinearProblemSolver<GO,LS,Z> SLP;
  SLP slp(go,ls,z,1e-10);
  slp.apply(); // here all the work is done!

  // Lagrange interpolation of exact solution
  Z w(gfs); //A coefficient Vector of type Z
  Dune::PDELab::interpolate(g,gfs,w);
  ZDGF wdgf(gfs,w); //Make a grid function out of it

  // Write VTK output file
  Dune::VTKWriter<GV> vtkwriter(gv,Dune::VTK::conforming);
  typedef Dune::PDELab::VTKGridFunctionAdapter<ZDGF> VTKF;
  vtkwriter.addVertexData(std::shared_ptr<VTKF>(new
                                         VTKF(zdgf,"u")));
  vtkwriter.addVertexData(std::shared_ptr<VTKF>(new
                                         VTKF(wdgf,"exact")));
  vtkwriter.write(ptree.get("output.filename","output"),
                  Dune::VTK::appendedraw);



  //Make a discrete grid function from the gradient of the calculated solution
  typedef Dune::PDELab::DiscreteGridFunctionGradient<GFS,Z> ZDGFGradient;
  ZDGFGradient zdgfgradient(gfs,z);

  //Make a (not descrete) grid function from the gradient of the analytical solution
  auto gradientOfGLambda = [&](const auto& e, const auto& x){return model.solutionGradient(e,x);};
  auto gradientOfG = Dune::PDELab::makeGridFunctionFromCallable(gv,gradientOfGLambda);

  //Make a grid function out of the pointwise squared error between the calculated and analytical solutions
  auto pointwise_squared_error = Dune::PDELab::DifferenceSquaredAdapter<decltype(g), ZDGF>(g, zdgf);

  //Integrate to obtain the suared L2-error between the calculated and analytical solutions
  Dune::FieldVector<double, 1> error (0.0);
  Dune::PDELab::integrateGridFunction(pointwise_squared_error, error, 2*DEGREE);

  //Make a grid function out of the pointwise squared error between the gradients of the calculated and analytical solutions
  auto pointwise_squared_gradient_error = Dune::PDELab::DifferenceSquaredAdapter<decltype(gradientOfG), ZDGFGradient>(gradientOfG, zdgfgradient);

  //Integrate to obtain the squared L2-error between the gradients of the calculated and analytical solutions
  Dune::FieldVector<double, 1> gradient_error (0.0);
  Dune::PDELab::integrateGridFunction(pointwise_squared_gradient_error, gradient_error, 2*DEGREE);

  //Calculate L2 error and H1 error by taking the appropiate square roots of the integrals above
  Dune::FieldVector<double, 1> H1error (0.0);
  H1error = sqrt(error+gradient_error);
  error = sqrt(error);


  //Output our errors and the refinement level
  int refinement = ptree.get<int>("grid.refinement");
  std::cout << "L2-error=" << error << std::endl;
  std::cout << "H1-error=" << H1error << std::endl;
  std::cout << "Refinement=" << refinement << std::endl;


  //Run test specified in ini file
  auto testtype = ptree.get<std::string>("testtype");

  if (testtype == "L2-test")
  {
    double tolerance = ptree.get<double>("tolerance");
    std::cout << "Running L2-test with tolerance=" << tolerance << std::endl;
    if (error > tolerance) {
      std::cout << "L2 error greater than tolerance." << std::endl;
      return 1;
    }
    else {
      std::cout << "Test passed" << std::endl;
      return 0;
    }
  }

  else if (testtype == "H1-test")
  {
    double tolerance = ptree.get<double>("tolerance");
    std::cout << "Running H1-test with tolerance=" << tolerance << std::endl;
    if (error > tolerance) {
      std::cout << "H1 error greater than tolerance." << std::endl;
      return 1;
    }
    else {
      std::cout << "Test passed" << std::endl;
      return 0;
    }
  }

  else if (testtype == "convergencetest")
  {
    std::cout << "Running convergencetest" << std::endl;
    RF hmax = pow(2, -refinement);
    //write convergence data in output tree
    Dune::OutputTree outputTree(ptree);
    outputTree.setConvergenceData(error, hmax, "L2");
    outputTree.setConvergenceData(H1error, hmax, "H1");
    return 0;
  }

  else
  {
    std::cout << "Unknown testtype. Possible choices are 'L2-test', 'H1-test', 'Convergencetest'." << std::endl;
  }
  return 0;
  return 0;
}
