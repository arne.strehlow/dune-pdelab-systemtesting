// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_PDELAB_SYSTEMTESTING_CONVECTIONDIFFUSIONPARAMETER_SAMPLES_HH
#define DUNE_PDELAB_SYSTEMTESTING_CONVECTIONDIFFUSIONPARAMETER_SAMPLES_HH

#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/geometry/type.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/quadraturerules.hh>
#include<dune/pdelab/common/geometrywrapper.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/functionutilities.hh>
#include<dune/pdelab/constraints/common/constraintsparameters.hh>
#include<dune/pdelab/localoperator/convectiondiffusionparameter.hh>
#include<math.h>

namespace Dune {
  namespace PDELab {

    /** \brief XXX Parameter class for solving the linear convection-diffusion equation
     *
     * A parameter class for the linear convection-diffusion equation
     * \f{align*}{
     *   \nabla\cdot(-A(x) \nabla u + b(x) u) + c(x)u &=& f \mbox{ in } \Omega,  \\
     *                                              u &=& g \mbox{ on } \partial\Omega_D \\
     *                (b(x) u - A(x)\nabla u) \cdot n &=& j \mbox{ on } \partial\Omega_N \\
     *                        -(A(x)\nabla u) \cdot n &=& o \mbox{ on } \partial\Omega_O
     * \f}
     * Note:
     *  - This formulation is valid for velocity fields which are non-divergence free.
     *  - Outflow boundary conditions should only be set on the outflow boundary
     *
     * \tparam GV The GridView type
     * \tparam RF The range field type
     */




    //Sample 0: Constant A,b,c, Dirichlet boundary conditions.
    template<typename GV, typename RF>
    class ConvectionDiffusionModelProblemSample0
    {
      typedef ConvectionDiffusionBoundaryConditions::Type BCType;

    public:
      typedef ConvectionDiffusionParameterTraits<GV,RF> Traits;

      //Dimension
      int dimension() {return 2;}

      static bool permeabilityIsConstantPerCell()
      {
        return true;
      }

      //! tensor diffusion coefficient
      typename Traits::PermTensorType
      A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::PermTensorType I;
        typename Traits::DomainType xglobal = e.geometry().global(x);
        I[0][0] = 1.0;
        I[0][1] = 0.0;
        I[1][0] = 0.0;
        I[1][1] = 1.0;
        //for (std::size_t i=0; i<Traits::dimDomain; i++)
        //  for (std::size_t j=0; j<Traits::dimDomain; j++)
        //  I[i][j] = (i==j) ? 1 : 0;
        return I;
      }

      //! velocity field
      typename Traits::RangeType
      b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        typename Traits::RangeType v;
        v[0] = 0.0;
        v[1] = 0.0;
        return v;
      }

      //! sink term
      typename Traits::RangeFieldType
      c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        return 0.0;
      }

      //! source term
      typename Traits::RangeFieldType
      f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return  - exp( (-1.0)*xglobal[0]*xglobal[0] - xglobal[1]*xglobal[1]) * (4.0*xglobal[0]*xglobal[0] + 4.0*xglobal[1]*xglobal[1] - 4.0);
      }


      //! boundary condition type function
      BCType
      bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return ConvectionDiffusionBoundaryConditions::Dirichlet;

      }

      //! automatic distinction between inflow and outflow boundary condition
      BCType
      Inflow_or_Outflow (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::RangeType evalb = b(is.inside(), is.geometryInInside().global(x));
        typename Traits::RangeFieldType flux = evalb * is.unitOuterNormal(x);
        if (flux > 0) {return ConvectionDiffusionBoundaryConditions::Outflow;}
        return ConvectionDiffusionBoundaryConditions::Neumann;
      }

      //! Dirichlet boundary condition value
      typename Traits::RangeFieldType
      g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return exp((-1.0)*xglobal[0]*xglobal[0] - xglobal[1]*xglobal[1]);
      }

      //! Gradient of analytical solution
      typename Traits::RangeType
      solutionGradient (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        typename Traits::RangeType gradient;

        gradient[0] = -2.0*xglobal[0]*exp((-1.0)*xglobal[0]*xglobal[0] - xglobal[1]*xglobal[1]);
        gradient[1] = -2.0*xglobal[1]*exp((-1.0)*xglobal[0]*xglobal[0] - xglobal[1]*xglobal[1]);

        return gradient;
      }

      //! Neumann boundary condition
      typename Traits::RangeFieldType
      j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }

      //! outflow boundary condition
      typename Traits::RangeFieldType
      o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }
    };




    //Sample 1: Constant A,b,c, Dirichlet boundary conditions.
    template<typename GV, typename RF>
    class ConvectionDiffusionModelProblemSample1
    {
      typedef ConvectionDiffusionBoundaryConditions::Type BCType;

    public:
      typedef ConvectionDiffusionParameterTraits<GV,RF> Traits;

      //Dimension
      int dimension() {return 2;}

      //! tensor diffusion coefficient
      typename Traits::PermTensorType
      A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::PermTensorType I;
        typename Traits::DomainType xglobal = e.geometry().global(x);
        I[0][0] = 5.0;
        I[0][1] = 2.0;
        I[1][0] = 2.0;
        I[1][1] = 1.0;
        //for (std::size_t i=0; i<Traits::dimDomain; i++)
        //  for (std::size_t j=0; j<Traits::dimDomain; j++)
        //  I[i][j] = (i==j) ? 1 : 0;
        return I;
      }

      //! velocity field
      typename Traits::RangeType
      b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        typename Traits::RangeType v;
        v[0] = 1.0;
        v[1] = 1.0;
        return v;
      }

      //! sink term
      typename Traits::RangeFieldType
      c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        return 1.0;
      }

      //! source term
      typename Traits::RangeFieldType
      f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return  -12.0 - 8.0*xglobal[0] - 10.0*xglobal[1]       + 2.0*xglobal[0] + 2.0*xglobal[0]*xglobal[1] + xglobal[0]*xglobal[0] + 2.0*xglobal[1]         + xglobal[0]*xglobal[0] + xglobal[0]*xglobal[0]*xglobal[1] + xglobal[1]*xglobal[1];
      }

      //! boundary condition type function
      BCType
      bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return ConvectionDiffusionBoundaryConditions::Dirichlet;

      }

      //! automatic distinction between inflow and outflow boundary condition
      BCType
      Inflow_or_Outflow (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::RangeType evalb = b(is.inside(), is.geometryInInside().global(x));
        typename Traits::RangeFieldType flux = evalb * is.unitOuterNormal(x);
        if (flux > 0) {return ConvectionDiffusionBoundaryConditions::Outflow;}
        return ConvectionDiffusionBoundaryConditions::Neumann;
      }


      //! Dirichlet boundary condition value
      typename Traits::RangeFieldType
      g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return xglobal[0]*xglobal[0] + xglobal[0]*xglobal[0]*xglobal[1] + xglobal[1]*xglobal[1];
      }

      //! Neumann boundary condition
      typename Traits::RangeFieldType
      j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }

      //! outflow boundary condition
      typename Traits::RangeFieldType
      o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }
    };





    //Sample 2: Constant A,b,c. Dirichlet + Neumann + outflow boundary conditions.
    template<typename GV, typename RF>
    class ConvectionDiffusionModelProblemSample2
    {
      typedef ConvectionDiffusionBoundaryConditions::Type BCType;

    public:
      typedef ConvectionDiffusionParameterTraits<GV,RF> Traits;

      //Dimension
      int dimension() {return 2;}

      //! tensor diffusion coefficient
      typename Traits::PermTensorType
      A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::PermTensorType I;
        typename Traits::DomainType xglobal = e.geometry().global(x);
        I[0][0] = 5.0;
        I[0][1] = 2.0;
        I[1][0] = 2.0;
        I[1][1] = 1.0;
        //for (std::size_t i=0; i<Traits::dimDomain; i++)
        //  for (std::size_t j=0; j<Traits::dimDomain; j++)
        //  I[i][j] = (i==j) ? 1 : 0;
        return I;
      }

      //! velocity field
      typename Traits::RangeType
      b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        typename Traits::RangeType v;
        v[0] = 1.0;
        v[1] = 1.0;
        return v;
      }

      //! sink term
      typename Traits::RangeFieldType
      c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        return 1.0;
      }

      //! source term
      typename Traits::RangeFieldType
      f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return  -12.0 - 8.0*xglobal[0] - 10.0*xglobal[1]       + 2.0*xglobal[0] + 2.0*xglobal[0]*xglobal[1] + xglobal[0]*xglobal[0] + 2.0*xglobal[1]         + xglobal[0]*xglobal[0] + xglobal[0]*xglobal[0]*xglobal[1] + xglobal[1]*xglobal[1];
      }


      //! boundary condition type function
      BCType
      bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::DomainType xglobal = is.geometry().global(x);
        if (xglobal[1]-xglobal[0] > 0)
        {
          return ConvectionDiffusionBoundaryConditions::Dirichlet;
        }
        else
        {
          return Inflow_or_Outflow(is,x);
        }
      }

      //! automatic distinction between inflow and outflow boundary condition
      BCType
      Inflow_or_Outflow (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::RangeType evalb = b(is.inside(), is.geometryInInside().global(x));
        typename Traits::RangeFieldType flux = evalb * is.unitOuterNormal(x);
        if (flux > 0) {return ConvectionDiffusionBoundaryConditions::Outflow;}
        return ConvectionDiffusionBoundaryConditions::Neumann;
      }

      //! Dirichlet boundary condition value
      typename Traits::RangeFieldType
      g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return xglobal[0]*xglobal[0] + xglobal[0]*xglobal[0]*xglobal[1] + xglobal[1]*xglobal[1];

      }

      //! Neumann boundary condition
      typename Traits::RangeFieldType
      j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::DomainType xglobal = is.geometry().global(x);
        typename Traits::RangeType gradient; //This will be A applied to the gradient of u: gradient = (A(x)\nabla u).
        gradient[0] = 10.0*xglobal[0] + 10.0*xglobal[0]*xglobal[1] + 2.0*xglobal[0]*xglobal[0] + 4.0*xglobal[1];
        gradient[1] = 4.0*xglobal[0] + 4.0*xglobal[0]*xglobal[1] + xglobal[0]*xglobal[0] + 2.0*xglobal[1];
        auto normal = is.unitOuterNormal(x);
        return - (normal*gradient)  + (normal[0] + normal[1]) * (xglobal[0]*xglobal[0] + xglobal[0]*xglobal[0]*xglobal[1] + xglobal[1]*xglobal[1]);
      }

      //! outflow boundary condition
      typename Traits::RangeFieldType
      o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::DomainType xglobal = is.geometry().global(x);
        typename Traits::RangeType gradient; //This will be A applied to the gradient of u: gradient = (A(x)\nabla u).
        gradient[0] = 10.0*xglobal[0] + 10.0*xglobal[0]*xglobal[1] + 2*xglobal[0]*xglobal[0] + 4.0*xglobal[1];
        gradient[1] = 4.0*xglobal[0] + 4.0*xglobal[0]*xglobal[1] + xglobal[0]*xglobal[0] + 2.0*xglobal[1];
        auto normal = is.unitOuterNormal(x);
        return -(normal*gradient);
      }
    };





    //Sample 3: Non-Constant A, b, c, Dirichlet boundary conditions.
    template<typename GV, typename RF>
    class ConvectionDiffusionModelProblemSample3
    {
      typedef ConvectionDiffusionBoundaryConditions::Type BCType;
      const double lambda = 2.0;

    public:
      typedef ConvectionDiffusionParameterTraits<GV,RF> Traits;

      //Dimension
      int dimension() {return 2;}

      //! tensor diffusion coefficient
      typename Traits::PermTensorType
      A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::PermTensorType I;
        typename Traits::DomainType xglobal = e.geometry().global(x);
        I[0][0] = exp(lambda*cos(13*xglobal[1]));
        I[0][1] = 0;
        I[1][0] = 0;
        I[1][1] = exp(lambda*cos(17*xglobal[0]));
        //for (std::size_t i=0; i<Traits::dimDomain; i++)
        //  for (std::size_t j=0; j<Traits::dimDomain; j++)
        //  I[i][j] = (i==j) ? 1 : 0;
        return I;
      }

      //! velocity field
      typename Traits::RangeType
      b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        typename Traits::RangeType v;
        v[0] = 1.0*cos(11.0*xglobal[0]);
        v[1] = 1.0*sin(7.0*xglobal[1]);
        return v;
      }

      //! sink term
      typename Traits::RangeFieldType
      c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return 3.0*cos(23.0*xglobal[0])*sin(29*xglobal[1]);
      }

      //! source term
      typename Traits::RangeFieldType
      f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return  -2.0*exp(lambda*cos(13.0*xglobal[1])) - 2.0*exp(lambda*cos(17.0*xglobal[0])) + 2.0*xglobal[0]*cos(11.0*xglobal[0]) + 2.0*xglobal[1]*sin(7.0*xglobal[1]) + (xglobal[0]*xglobal[0] + xglobal[1]*xglobal[1])*(-11.0*sin(11.0*xglobal[0]) + 7.0*cos(7.0*xglobal[1])) + 3.0*cos(23.0*xglobal[0])*sin(29.0*xglobal[1]) * (xglobal[0]*xglobal[0] + xglobal[1]*xglobal[1]);
      }

      //! Boundary condition type
      BCType
      bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return ConvectionDiffusionBoundaryConditions::Dirichlet;

      }

      //! automatic distinction between inflow and outflow boundary condition
      BCType
      Inflow_or_Outflow (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::RangeType evalb = b(is.inside(), is.geometryInInside().global(x));
        typename Traits::RangeFieldType flux = evalb * is.unitOuterNormal(x);
        if (flux > 0) {return ConvectionDiffusionBoundaryConditions::Outflow;}
        return ConvectionDiffusionBoundaryConditions::Neumann;
      }


      //! Dirichlet boundary condition value
      typename Traits::RangeFieldType
      g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return xglobal[0]*xglobal[0] + xglobal[1]*xglobal[1];

      }

      //! Neumann boundary condition
      typename Traits::RangeFieldType
      j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::DomainType xglobal = is.geometry().global(x);
        typename Traits::RangeType gradient; //This will be A applied to the gradient of u: gradient = (A(x)\nabla u).
        gradient[0] = 2.0*xglobal[0]*exp(lambda*cos(13.0*xglobal[1]));
        gradient[1] = 2.0*xglobal[1]*exp(lambda*cos(17.0*xglobal[0]));
        auto normal = is.unitOuterNormal(x);
        auto evalg = g(is.inside(), is.geometryInInside().global(x));
        return - normal[0]*gradient[0] - normal[1]*gradient[1] + (1.0*cos(11.0*xglobal[0])*normal[0] + 1.0*sin(7.0*xglobal[1])*normal[1]) * (xglobal[0]*xglobal[0] + xglobal[1]*xglobal[1]);
      }

      //! outflow boundary condition
      typename Traits::RangeFieldType
      o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::DomainType xglobal = is.geometry().global(x);
        typename Traits::RangeType gradient; //This will be A applied to the gradient of u: gradient = (A(x)\nabla u).
        gradient[0] = 2.0*xglobal[0]*exp(lambda*cos(13.0*xglobal[1]));
        gradient[1] = 2.0*xglobal[1]*exp(lambda*cos(17.0*xglobal[0]));
        auto normal = is.unitOuterNormal(x);
        return - normal[0]*gradient[0] - normal[1]*gradient[1] + (1.0*cos(11.0*xglobal[0])*normal[0] + 1.0*sin(7.0*xglobal[1])*normal[1]) * (xglobal[0]*xglobal[0] + xglobal[1]*xglobal[1]);
      }
    };





    //Sample 4: Non-Constant A, b=0 ,c=0, Dirichlet boundary conditions.
    template<typename GV, typename RF>
    class ConvectionDiffusionModelProblemSample4
    {
      typedef ConvectionDiffusionBoundaryConditions::Type BCType;
      const double lambda = 1.0; //this parameter appears in A. Higher lambda gives a greater range of Eigenvalues of A.

    public:
      typedef ConvectionDiffusionParameterTraits<GV,RF> Traits;

      //Dimension
      int dimension() {return 2;}

      //! tensor diffusion coefficient
      typename Traits::PermTensorType
      A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::PermTensorType I;
        typename Traits::DomainType xglobal = e.geometry().global(x);
        I[0][0] = exp(lambda*cos(13*xglobal[0]));
        I[0][1] = 0;
        I[1][0] = 0;
        I[1][1] = exp(lambda*cos(17*xglobal[1]));
        //for (std::size_t i=0; i<Traits::dimDomain; i++)
        //  for (std::size_t j=0; j<Traits::dimDomain; j++)
        //  I[i][j] = (i==j) ? 1 : 0;
        return I;
      }

      //! velocity field
      typename Traits::RangeType
      b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        typename Traits::RangeType v;
        v[0] = 0.0;
        v[1] = 0.0;
        return v;
      }

      //! sink term
      typename Traits::RangeFieldType
      c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        return 0.0;
      }

      //! source term
      typename Traits::RangeFieldType
      f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return  -2.0*exp(lambda*cos(13.0*xglobal[0])) + 26.0*xglobal[0]*exp(lambda*cos(13.0*xglobal[0]))*lambda*sin(13.0*xglobal[0]) - 2.0*exp(lambda*cos(17.0*xglobal[1])) + 34.0*xglobal[1]*exp(lambda*cos(17*xglobal[1]))*lambda*sin(17.0*xglobal[1]);
      }

      //! Boundary condition type
      BCType
      bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return ConvectionDiffusionBoundaryConditions::Dirichlet;

      }

      //! automatic distinction between inflow and outflow boundary condition
      BCType
      Inflow_or_Outflow (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::RangeType evalb = b(is.inside(), is.geometryInInside().global(x));
        typename Traits::RangeFieldType flux = evalb * is.unitOuterNormal(x);
        if (flux > 0) {return ConvectionDiffusionBoundaryConditions::Outflow;}
        return ConvectionDiffusionBoundaryConditions::Neumann;
      }

      //! Dirichlet boundary condition value
      typename Traits::RangeFieldType
      g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return xglobal[0]*xglobal[0] + xglobal[1]*xglobal[1];

      }

      //! Neumann boundary condition
      typename Traits::RangeFieldType
      j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }

      //! outflow boundary condition
      typename Traits::RangeFieldType
      o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }
    };


    //Sample 5: Non-Constant A, b=0 ,c=0, Dirichlet boundary conditions. u of infinite order.
    template<typename GV, typename RF>
    class ConvectionDiffusionModelProblemSample5
    {
      typedef ConvectionDiffusionBoundaryConditions::Type BCType;
      const double lambda = 1.0; //this parameter appears in A. Higher lambda gives a greater range of Eigenvalues of A.
      const double alpha = 0.1; //this parameter appears in A. Higher alpha means a faster oscillating A.

    public:
      typedef ConvectionDiffusionParameterTraits<GV,RF> Traits;

      //Dimension
      int dimension() {return 2;}

      //! tensor diffusion coefficient
      typename Traits::PermTensorType
      A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::PermTensorType I;
        typename Traits::DomainType xglobal = e.geometry().global(x);
        I[0][0] = exp(lambda*cos(alpha*xglobal[1]));
        I[0][1] = 0;
        I[1][0] = 0;
        I[1][1] = exp(lambda*cos(alpha*xglobal[0]));
        //for (std::size_t i=0; i<Traits::dimDomain; i++)
        //  for (std::size_t j=0; j<Traits::dimDomain; j++)
        //  I[i][j] = (i==j) ? 1 : 0;
        return I;
      }

      //! velocity field
      typename Traits::RangeType
      b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        typename Traits::RangeType v;
        v[0] = 0.0;
        v[1] = 0.0;
        return v;
      }

      //! sink term
      typename Traits::RangeFieldType
      c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        return 0.0;
      }

      //! source term
      typename Traits::RangeFieldType
      f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        auto norm = xglobal[0]*xglobal[0] + xglobal[1]*xglobal[1];
        return  2.0*exp(-norm + lambda*cos(alpha*xglobal[1])) - 4.0*xglobal[0]*xglobal[0]*exp(-norm + lambda*cos(alpha*xglobal[1])) + 2.0*exp(-norm + lambda*cos(alpha*xglobal[0])) - 4.0*xglobal[1]*xglobal[1]*exp(-norm + lambda*cos(alpha*xglobal[0]));
      }


      //! Boundary condition type
      BCType
      bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return ConvectionDiffusionBoundaryConditions::Dirichlet;

      }

      //! automatic distinction between inflow and outflow boundary condition
      BCType
      Inflow_or_Outflow (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::RangeType evalb = b(is.inside(), is.geometryInInside().global(x));
        typename Traits::RangeFieldType flux = evalb * is.unitOuterNormal(x);
        if (flux > 0) {return ConvectionDiffusionBoundaryConditions::Outflow;}
        return ConvectionDiffusionBoundaryConditions::Neumann;
      }

      //! Dirichlet boundary condition value
      typename Traits::RangeFieldType
      g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return exp(-(xglobal[0]*xglobal[0] + xglobal[1]*xglobal[1]));

      }

      //! Neumann boundary condition
      typename Traits::RangeFieldType
      j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }

      //! outflow boundary condition
      typename Traits::RangeFieldType
      o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }
    };



    //Sample 5b: A depends only on x[0].
    template<typename GV, typename RF>
    class ConvectionDiffusionModelProblemSample5b
    {
      typedef ConvectionDiffusionBoundaryConditions::Type BCType;
      const double lambda = 1.0; //this parameter appears in A. Higher lambda gives a greater range of Eigenvalues of A.
      const double alpha = 1.0; //this parameter appears in A. Higher alpha means a faster oscillating A.

    public:
      typedef ConvectionDiffusionParameterTraits<GV,RF> Traits;

      static bool permeabilityIsConstantPerCell()
      {
        return false;
      }

      //Dimension
      int dimension() {return 2;}

      //! tensor diffusion coefficient
      typename Traits::PermTensorType
      A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::PermTensorType I;
        typename Traits::DomainType xglobal = e.geometry().global(x);
        I[0][0] = exp(lambda*cos(alpha*xglobal[0]));
        I[0][1] = 0;
        I[1][0] = 0;
        I[1][1] = exp(lambda*cos(alpha*xglobal[0]));
        //for (std::size_t i=0; i<Traits::dimDomain; i++)
        //  for (std::size_t j=0; j<Traits::dimDomain; j++)
        //  I[i][j] = (i==j) ? 1 : 0;
        return I;
      }

      //! velocity field
      typename Traits::RangeType
      b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        typename Traits::RangeType v;
        v[0] = 0.0;
        v[1] = 0.0;
        return v;
      }

      //! sink term
      typename Traits::RangeFieldType
      c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        return 0.0;
      }

      //! source term
      typename Traits::RangeFieldType
      f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        auto norm = xglobal[0]*xglobal[0] + xglobal[1]*xglobal[1];
        return  exp(lambda*cos(alpha*xglobal[0]) - norm)* ( 4.0 - 2.0*xglobal[0]*(alpha*lambda*sin(alpha*xglobal[0]) + 2.0*xglobal[0]) - 4.0*xglobal[1]*xglobal[1]);
      }


      //! Boundary condition type
      BCType
      bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return ConvectionDiffusionBoundaryConditions::Dirichlet;

      }

      //! automatic distinction between inflow and outflow boundary condition
      BCType
      Inflow_or_Outflow (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::RangeType evalb = b(is.inside(), is.geometryInInside().global(x));
        typename Traits::RangeFieldType flux = evalb * is.unitOuterNormal(x);
        if (flux > 0) {return ConvectionDiffusionBoundaryConditions::Outflow;}
        return ConvectionDiffusionBoundaryConditions::Neumann;
      }

      //! Dirichlet boundary condition value
      typename Traits::RangeFieldType
      g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return exp(-(xglobal[0]*xglobal[0] + xglobal[1]*xglobal[1]));

      }

      typename Traits::RangeType
      solutionGradient (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        typename Traits::RangeType gradient;
        gradient[0] = -2.0*xglobal[0]*g(e,x);
        gradient[1] = -2.0*xglobal[1]*g(e,x);
        return gradient;
      }

      //! Neumann boundary condition
      typename Traits::RangeFieldType
      j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }

      //! outflow boundary condition
      typename Traits::RangeFieldType
      o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }
    };


    //Sample 5c: A depends only on x[0]. Added convection b.
    template<typename GV, typename RF>
    class ConvectionDiffusionModelProblemSample5c
    {
      typedef ConvectionDiffusionBoundaryConditions::Type BCType;
      const double lambda = 1.0; //this parameter appears in A. Higher lambda gives a greater range of Eigenvalues of A.
      const double alpha = 1.0; //this parameter appears in A. Higher alpha means a faster oscillating A.
      const double theta = 1.0; //this parameter appears in b. Equals maximum-norm of b.
      const double omega = 1.0; // this parameter appears in the definition of b. Higher omega means faster oscillating b.

    public:
      typedef ConvectionDiffusionParameterTraits<GV,RF> Traits;

      static bool permeabilityIsConstantPerCell()
      {
        return false;
      }

      //Dimension
      int dimension() {return 2;}

      //! tensor diffusion coefficient
      typename Traits::PermTensorType
      A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::PermTensorType I;
        typename Traits::DomainType xglobal = e.geometry().global(x);
        I[0][0] = exp(lambda*cos(alpha*xglobal[0]));
        I[0][1] = 0;
        I[1][0] = 0;
        I[1][1] = exp(lambda*cos(alpha*xglobal[0]));
        //for (std::size_t i=0; i<Traits::dimDomain; i++)
        //  for (std::size_t j=0; j<Traits::dimDomain; j++)
        //  I[i][j] = (i==j) ? 1 : 0;
        return I;
      }

      //! velocity field
      typename Traits::RangeType
      b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        typename Traits::RangeType v;
        v[0] = theta*cos(omega*xglobal[0]);
        v[1] = theta*sin(omega*xglobal[1]);
        return v;
      }

      //! sink term
      typename Traits::RangeFieldType
      c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        return 0.0;
      }

      //! source term
      typename Traits::RangeFieldType
      f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        auto norm = xglobal[0]*xglobal[0] + xglobal[1]*xglobal[1];
        return  exp(lambda*cos(alpha*xglobal[0]) - norm)* ( 4.0 - 2.0*xglobal[0]*(alpha*lambda*sin(alpha*xglobal[0]) + 2.0*xglobal[0]) - 4.0*xglobal[1]*xglobal[1]) + b(e,x) * solutionGradient(e,x) + g(e,x) * theta * omega * (-sin(omega*xglobal[0]) + cos(omega*xglobal[1]));
      }


      //! Boundary condition type
      BCType
      bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return ConvectionDiffusionBoundaryConditions::Dirichlet;

      }

      //! automatic distinction between inflow and outflow boundary condition
      BCType
      Inflow_or_Outflow (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::RangeType evalb = b(is.inside(), is.geometryInInside().global(x));
        typename Traits::RangeFieldType flux = evalb * is.unitOuterNormal(x);
        if (flux > 0) {return ConvectionDiffusionBoundaryConditions::Outflow;}
        return ConvectionDiffusionBoundaryConditions::Neumann;
      }

      //! Dirichlet boundary condition value
      typename Traits::RangeFieldType
      g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return exp(-(xglobal[0]*xglobal[0] + xglobal[1]*xglobal[1]));

      }

      typename Traits::RangeType
      solutionGradient (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        typename Traits::RangeType gradient;
        gradient[0] = -2.0*xglobal[0]*g(e,x);
        gradient[1] = -2.0*xglobal[1]*g(e,x);
        return gradient;
      }

      //! Neumann boundary condition
      typename Traits::RangeFieldType
      j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }

      //! outflow boundary condition
      typename Traits::RangeFieldType
      o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }
    };


    //Sample 6: Dimension 3. Non-Constant A, b=(1,1,1) ,c=1, Dirichlet boundary conditions. u of infinite order.
    template<typename GV, typename RF>
    class ConvectionDiffusionModelProblemSample6
    {
      typedef ConvectionDiffusionBoundaryConditions::Type BCType;
      const double lambda = 1.0; //this parameter appears in A. Higher lambda gives a greater range of Eigenvalues of A.

    public:
      typedef ConvectionDiffusionParameterTraits<GV,RF> Traits;

      //Dimension
      int dimension() {return 3;}

      //! tensor diffusion coefficient
      typename Traits::PermTensorType
      A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::PermTensorType I;
        typename Traits::DomainType xglobal = e.geometry().global(x);
        I[0][0] = exp(lambda*cos(13.0*xglobal[1]));
        I[0][1] = 0;
        I[0][2] = 0;
        I[1][0] = 0;
        I[1][1] = exp(lambda*cos(17.0*xglobal[2]));
        I[1][2] = 0;
        I[2][0] = 0;
        I[2][1] = 0;
        I[2][2] = exp(lambda*cos(11.0*xglobal[0]));
        //for (std::size_t i=0; i<Traits::dimDomain; i++)
        //  for (std::size_t j=0; j<Traits::dimDomain; j++)
        //  I[i][j] = (i==j) ? 1 : 0;
        return I;
      }

      //! velocity field
      typename Traits::RangeType
      b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        typename Traits::RangeType v;
        v[0] = 0.0;
        v[1] = 0.0;
        v[2] = 0.0;
        return v;
      }

      //! sink term
      typename Traits::RangeFieldType
      c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        return 0.0;
      }

      //! source term
      typename Traits::RangeFieldType
      f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return  -exp(xglobal[0] + xglobal[1] + xglobal[2]) * ( exp(lambda * cos(13.0 * xglobal[1])) + exp(lambda * cos(17.0 * xglobal[2])) + exp(lambda* cos(11.0 * xglobal[0])) );
      }


      //Boundary condition type
      BCType
      bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return ConvectionDiffusionBoundaryConditions::Dirichlet;

      }

      //! automatic distinction between inflow and outflow boundary condition
      BCType
      Inflow_or_Outflow (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::RangeType evalb = b(is.inside(), is.geometryInInside().global(x));
        typename Traits::RangeFieldType flux = evalb * is.unitOuterNormal(x);
        if (flux > 0) {return ConvectionDiffusionBoundaryConditions::Outflow;}
        return ConvectionDiffusionBoundaryConditions::Neumann;
      }

      //! Dirichlet boundary condition value
      typename Traits::RangeFieldType
      g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return exp(xglobal[0] + xglobal[1] + xglobal[2]);

      }

      //! Neumann boundary condition
      typename Traits::RangeFieldType
      j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }

      //! outflow boundary condition
      typename Traits::RangeFieldType
      o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }
    };



    //Sample 7: Dimension 3. Constant and full  A, b=(1,1,1) ,c=1, Neumann and Outflow boundary conditions. u of infinite order.
    template<typename GV, typename RF>
    class ConvectionDiffusionModelProblemSample7
    {
      typedef ConvectionDiffusionBoundaryConditions::Type BCType;
      const double lambda = 1.0; //this parameter appears in A. Higher lambda gives a greater range of Eigenvalues of A.

    public:
      typedef ConvectionDiffusionParameterTraits<GV,RF> Traits;

      //Dimension
      int dimension() {return 3;}

      //! tensor diffusion coefficient
      typename Traits::PermTensorType
      A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::PermTensorType I;
        typename Traits::DomainType xglobal = e.geometry().global(x);
        I[0][0] = 3.0;
        I[0][1] = 2.0;
        I[0][2] = 1.0;
        I[1][0] = 2.0;
        I[1][1] = 2.0;
        I[1][2] = 1.0;
        I[2][0] = 1.0;
        I[2][1] = 1.0;
        I[2][2] = 1.0;
        //for (std::size_t i=0; i<Traits::dimDomain; i++)
        //  for (std::size_t j=0; j<Traits::dimDomain; j++)
        //  I[i][j] = (i==j) ? 1 : 0;
        return I;
      }

      //! velocity field
      typename Traits::RangeType
      b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        typename Traits::RangeType v;
        v[0] = 1.0;
        v[1] = 1.0;
        v[2] = 1.0;
        return v;
      }

      //! sink term
      typename Traits::RangeFieldType
      c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        return 1.0;
      }

      //! source term
      typename Traits::RangeFieldType
      f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return  -10.0 * exp(xglobal[0] + xglobal[1] + xglobal[2]);
      }

      //! boundary condition type function
      BCType
      bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return Inflow_or_Outflow(is,x);

      }

      //! automatic distinction between inflow and outflow boundary condition
      BCType
      Inflow_or_Outflow (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::RangeType evalb = b(is.inside(), is.geometryInInside().global(x));
        typename Traits::RangeFieldType flux = evalb * is.unitOuterNormal(x);
        if (flux > 0) {return ConvectionDiffusionBoundaryConditions::Outflow;}
        return ConvectionDiffusionBoundaryConditions::Neumann;
      }

      //! Dirichlet boundary condition value
      typename Traits::RangeFieldType
      g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return exp(xglobal[0] + xglobal[1] + xglobal[2]);

      }

      //! Neumann boundary condition
      typename Traits::RangeFieldType
      j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::DomainType xglobal = is.geometry().global(x);
        typename Traits::RangeType Agradient; //This will be A applied to the gradient of u: gradient = (A(x)\nabla u).
        Agradient[0] = 6.0 * exp(xglobal[0] + xglobal[1] + xglobal[2]);
        Agradient[1] = 5.0 * exp(xglobal[0] + xglobal[1] + xglobal[2]);
        Agradient[2] = 3.0 * exp(xglobal[0] + xglobal[1] + xglobal[2]);
        auto normal = is.unitOuterNormal(x);
        auto evalb = b(is.inside(), is.geometryInInside().global(x));
        auto evalg = g(is.inside(), is.geometryInInside().global(x));
        return -1.0*(normal * Agradient) + (normal * evalb) * evalg;
      }

      //! outflow boundary condition
      typename Traits::RangeFieldType
      o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::DomainType xglobal = is.geometry().global(x);
        typename Traits::RangeType Agradient; //This will be A applied to the gradient of u: gradient = (A(x)\nabla u).
        Agradient[0] = 6.0 * exp(xglobal[0] + xglobal[1] + xglobal[2]);
        Agradient[1] = 5.0 * exp(xglobal[0] + xglobal[1] + xglobal[2]);
        Agradient[2] = 3.0 * exp(xglobal[0] + xglobal[1] + xglobal[2]);
        auto normal = is.unitOuterNormal(x);
        auto evalb = b(is.inside(), is.geometryInInside().global(x));
        auto evalg = g(is.inside(), is.geometryInInside().global(x));
        return -1.0*(normal * Agradient);
      }
    };


    //Sample 8: Dimension 3.
    template<typename GV, typename RF>
    class ConvectionDiffusionModelProblemSample8
    {
      typedef ConvectionDiffusionBoundaryConditions::Type BCType;
      const double amax; // this parameter appears in A. Higher lambda gives a greater range of Eigenvalues of A.
      const double avar; // this parameter appears in A. Higher alpha means a faster oscillating A.
      const double cmax; // maximum of c
      const double cvar; // variability of c
      const double bmax; // maximum of b
      const double bvar; // variability of b
      const double ovar; // rotation of permeability tensor around x0-axis by ovar*(x0+x1)
      const double pvar; // rotation of permeability tensor around x2-axis by pvar*x2

    public:
      ConvectionDiffusionModelProblemSample8(double amax_ = 1.0,
                                             double avar_ = 1.0,
                                             double cmax_ = 1.0,
                                             double cvar_ = 1.0,
                                             double bmax_ = 1.0,
                                             double bvar_ = 1.0,
                                             double ovar_ = 1.0,
                                             double pvar_ = 1.0
                                            )
        : amax(amax_), avar(avar_), cmax(cmax_), cvar(cvar_), bmax(bmax_), bvar(bvar_), ovar(ovar_), pvar(pvar_)
      {}
      typedef ConvectionDiffusionParameterTraits<GV,RF> Traits;

      static bool permeabilityIsConstantPerCell()
      {
        return false;
      }

      //Dimension
      int dimension() {return 3;}

      //! tensor diffusion coefficient
      typename Traits::PermTensorType
      A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::PermTensorType I;
        typename Traits::DomainType xglobal = e.geometry().global(x);
        auto x0 = xglobal[0];
        auto x1 = xglobal[1];
        auto x2 = xglobal[2];

        double x3 = pvar*x2;
        double x4 = cos(x3);
        double x5 = x4*x4;
        double x6 = exp(amax*cos(avar*x0));
        double x7 = sin(x3);
        double x8 = x7*x7;
        double x9 = exp(amax*cos(avar*x1));
        double x10 = ovar*(x0 + x1);
        double x11 = cos(x10);
        double x12 = x4*x7;
        double x13 = x12*x6;
        double x14 = x12*x9;
        double x15 = x13 + (-1)*x14;
        double x16 = sin(x10);
        double x17 = exp(amax*cos(avar*x2));
        double x18 = x6*x8;
        double x19 = x5*x9;
        double x20 = x11*x18 + x11*x19;
        double x21 = (-1)*x11*x16*x17;
        double x22 = x16*x18 + x16*x19;
        I[0][0] = x5*x6 + x8*x9;
        I[0][1] = x11*x15;
        I[0][2] = x15*x16;
        I[1][0] = x11*x13 + (-1)*x11*x14;
        I[1][1] = x11*x20 + x17*x16*x16;
        I[1][2] = x21 + x16*x20;
        I[2][0] = x13*x16 + (-1)*x14*x16;
        I[2][1] = x21 + x11*x22;
        I[2][2] = x16*x22 + x17*x11*x11;

        return I;
      }

      //! velocity field
      typename Traits::RangeType
      b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        auto x0 = xglobal[0];
        auto x1 = xglobal[1];
        auto x2 = xglobal[2];
        typename Traits::RangeType v;

        v[0] = bmax*cos(bvar*x0);
        v[1] = bmax*sin(bvar*x2);
        v[2] = bmax*cos(bvar*x1);

        return v;
      }

      //! sink term
      typename Traits::RangeFieldType
      c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        auto x0 = xglobal[0];
        auto x1 = xglobal[1];
        auto x2 = xglobal[2];
        return cmax*cos(cvar*x0)*cos(cvar*x1);
      }

      //! source term
      typename Traits::RangeFieldType
      f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        auto x0 = xglobal[0];
        auto x1 = xglobal[1];
        auto x2 = xglobal[2];

        double x3 = bvar*x0;
        double x4 = x0*x0;
        double x5 = x1*x1;
        double x6 = x2*x2;
        double x7 = exp((-1)*x4 + (-1)*x5 + (-1)*x6);
        double x8 = 2*x7;
        double x9 = x0*x8;
        double x10 = x1*x8;
        double x11 = x2*x8;
        double x12 = avar*x0;
        double x13 = pvar*x2;
        double x14 = cos(x13);
        double x15 = x14*x14;
        double x16 = exp(amax*cos(x12));
        double x17 = x15*x16;
        double x18 = 2*x17;
        double x19 = amax*sin(x12);
        double x20 = avar*x1;
        double x21 = ovar*(x0 + x1);
        double x22 = cos(x21);
        double x23 = sin(x13);
        double x24 = x14*x23;
        double x25 = x16*x24;
        double x26 = x22*x25;
        double x27 = x19*x8;
        double x28 = avar*x2;
        double x29 = sin(x21);
        double x30 = x25*x29;
        double x31 = x23*x23;
        double x32 = exp(amax*cos(x20));
        double x33 = x31*x32;
        double x34 = 4*x7;
        double x35 = x24*x32;
        double x36 = x25 + (-1)*x35;
        double x37 = x29*x36;
        double x38 = x22*x36;
        double x39 = x0*x34;
        double x40 = x1*x39;
        double x41 = x2*x39;
        double x42 = x22*x35;
        double x43 = x29*x35;
        double x44 = exp(amax*cos(x28));
        double x45 = x44*x22*x22;
        double x46 = x16*x31;
        double x47 = x29*x46;
        double x48 = x15*x32;
        double x49 = x29*x48;
        double x50 = x47 + x49;
        double x51 = x29*x50;
        double x52 = x44*x29*x29;
        double x53 = x22*x48;
        double x54 = x53 + x22*x46;
        double x55 = x22*x54;
        double x56 = x22*x29*x44;
        double x57 = (-1)*x56;
        double x58 = x1*x2*x34;
        double x59 = x29*x54;
        double x60 = amax*avar;
        double x61 = x60*sin(x20);
        double x62 = x60*sin(x28);
        double x63 = pvar*x29;
        double x64 = 2*x63;
        double x65 = x25*x64 + (-1)*x35*x64;
        double x66 = (-1)*ovar*x47 + (-1)*ovar*x49 + (-1)*x53*x61;
        return x10*(x22*x65 + x56*x62) + x10*(x22*x66 + (-1)*ovar*x59 + 2*ovar*x56) + x11*(x29*x65 + (-1)*x45*x62) + x11*(ovar*x52 + ovar*x55 + x29*x66 + (-1)*ovar*x45) + x9*(ovar*x43 + x42*x61 + (-1)*ovar*x30) + x9*(x17*x63 + x33*x63 + (-1)*pvar*x47 + (-1)*pvar*x49) + (-1)*x37*x41 + (-1)*x38*x40 + (-1)*x40*(x26 + (-1)*x42) + (-1)*x41*(x30 + (-1)*x43) + (-1)*x58*(x57 + x59) + (-1)*x58*(x57 + x22*x50) + (-1)*x7*((-1)*x18 + (-2)*x33) + (-1)*x7*((-2)*x45 + (-2)*x51) + (-1)*x7*((-2)*x52 + (-2)*x55) + ovar*x11*x38 + (-1)*bmax*x10*sin(bvar*x2) + (-1)*bmax*x11*cos(bvar*x1) + (-1)*bmax*x9*cos(x3) + (-1)*ovar*x10*x37 + (-1)*x20*x26*x27 + (-1)*x27*x28*x30 + (-1)*x34*x4*(x17 + x33) + (-1)*x34*x5*(x52 + x55) + (-1)*x34*x6*(x45 + x51) + cmax*x7*cos(cvar*x0)*cos(cvar*x1) + (-1)*bmax*bvar*x7*sin(x3) + (-1)*x12*x18*x19*x7;
      }


      //Boundary condition type
      BCType
      bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::DomainType xglobal = is.geometry().global(x);
        if (xglobal[0] + xglobal[1] > 1)
          return Inflow_or_Outflow(is,x);
        else
          return ConvectionDiffusionBoundaryConditions::Dirichlet;
      }

      //! automatic distinction between inflow and outflow boundary condition
      BCType
      Inflow_or_Outflow (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::RangeType evalb = b(is.inside(), is.geometryInInside().global(x));
        typename Traits::RangeFieldType flux = evalb * is.unitOuterNormal(x);
        if (flux > 0) {return ConvectionDiffusionBoundaryConditions::Outflow;}
        return ConvectionDiffusionBoundaryConditions::Neumann;
      }

      //! Dirichlet boundary condition value
      typename Traits::RangeFieldType
      g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        auto x0 = xglobal[0];
        auto x1 = xglobal[1];
        auto x2 = xglobal[2];
        return exp((-1)*(x2*x2 + x0*x0 + x1*x1));

      }

      //! Gradient of analytical solution
      typename Traits::RangeType
      solutionGradient (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        typename Traits::RangeType gradient;
        auto x0 = xglobal[0];
        auto x1 = xglobal[1];
        auto x2 = xglobal[2];

        double x3 = 2*exp((-1)*x0*x0 + (-1)*x1*x1 + (-1)*x2*x2);
        gradient[0] = (-1)*x0*x3;
        gradient[1] = (-1)*x1*x3;
        gradient[2] = (-1)*x2*x3;

        return gradient;
      }

      //! Neumann boundary condition
      typename Traits::RangeFieldType
      j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::DomainType xglobal = is.geometry().global(x);
        auto evalb = b(is.inside(), is.geometryInInside().global(x));
        auto evalA = A(is.inside(), is.geometryInInside().global(x));
        auto evalg = g(is.inside(), is.geometryInInside().global(x));
        auto normal = is.unitOuterNormal(x);
        auto evalgradient = solutionGradient(is.inside(), is.geometryInInside().global(x));
        auto Agradient = evalgradient;
        evalA.mv(evalgradient,Agradient);
        evalb *= evalg;
        return (evalb - Agradient) * normal;
      }

      //! outflow boundary condition
      typename Traits::RangeFieldType
      o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::DomainType xglobal = is.geometry().global(x);
        auto evalA = A(is.inside(), is.geometryInInside().global(x));
        auto normal = is.unitOuterNormal(x);
        auto evalgradient = solutionGradient(is.inside(), is.geometryInInside().global(x));
        auto Agradient = evalgradient;
        evalA.mv(evalgradient,Agradient);
        return -(Agradient * normal);
      }
    };

    //Sample 9: Dimension 2.

    template<typename GV, typename RF>
    class ConvectionDiffusionModelProblemSample9
    {
      typedef ConvectionDiffusionBoundaryConditions::Type BCType;
      const double amax; // this parameter appears in A. Higher lambda gives a greater range of Eigenvalues of A.
      const double avar; // this parameter appears in A. Higher alpha means a faster oscillating A.
      const double cmax; // maximum of c
      const double cvar; // variability of c
      const double bmax; // maximum of b
      const double bvar; // variability of b
      const double ovar; // rotation of permeability tensor around x0-axis by ovar*x0*x1

    public:

      ConvectionDiffusionModelProblemSample9(double amax_ = 1.0,
                                             double avar_ = 1.0,
                                             double cmax_ = 1.0,
                                             double cvar_ = 1.0,
                                             double bmax_ = 1.0,
                                             double bvar_ = 1.0,
                                             double ovar_ = 1.0,
                                             double placeholder_ = 0.0
                                            )
        : amax(amax_), avar(avar_), cmax(cmax_), cvar(cvar_), bmax(bmax_), bvar(bvar_), ovar(ovar_)
      {}

      typedef ConvectionDiffusionParameterTraits<GV,RF> Traits;

      static bool permeabilityIsConstantPerCell()
      {
        return false;
      }

      //Dimension
      int dimension() {return 2;}

      //! tensor diffusion coefficient
      typename Traits::PermTensorType
      A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::PermTensorType I;
        typename Traits::DomainType xglobal = e.geometry().global(x);
        auto x0 = xglobal[0];
        auto x1 = xglobal[1];

        double x2 = exp(amax*cos(avar*x0));
        double x3 = ovar*(x0 + x1);
        double x4 = cos(x3);
        double x5 = x4*x4;
        double x6 = exp(amax*cos(avar*x1));
        double x7 = sin(x3);
        double x8 = x7*x7;
        double x9 = x4*x7;
        double x10 = x2*x9 + (-1)*x6*x9;
        I[0][0] = x2*x5 + x6*x8;
        I[0][1] = x10;
        I[1][0] = x10;
        I[1][1] = x2*x8 + x5*x6;

        return I;
      }

      //! velocity field
      typename Traits::RangeType
      b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        auto x0 = xglobal[0];
        auto x1 = xglobal[1];

        typename Traits::RangeType v;

        v[0] = bmax*cos(bvar*x0);
        v[1] = bmax*sin(bvar*x1);

        return v;
      }

      //! sink term
      typename Traits::RangeFieldType
      c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        auto x0 = xglobal[0];
        auto x1 = xglobal[1];

        return cmax*cos(cvar*x0)*cos(cvar*x1);
      }

      //! source term
      typename Traits::RangeFieldType
      f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        auto x0 = xglobal[0];
        auto x1 = xglobal[1];

        double x2 = bvar*x1;
        double x3 = x0*x0;
        double x4 = x1*x1;
        double x5 = exp((-1)*x3 + (-1)*x4);
        double x6 = bmax*bvar*x5;
        double x7 = bvar*x0;
        double x8 = 2*x5;
        double x9 = x0*x8;
        double x10 = x1*x8;
        double x11 = avar*x0;
        double x12 = exp(amax*cos(x11));
        double x13 = ovar*(x0 + x1);
        double x14 = sin(x13);
        double x15 = x14*x14;
        double x16 = x12*x15;
        double x17 = avar*x1;
        double x18 = exp(amax*cos(x17));
        double x19 = cos(x13);
        double x20 = x19*x19;
        double x21 = x18*x20;
        double x22 = x12*x20;
        double x23 = x15*x18;
        double x24 = 4*x5;
        double x25 = x14*x19;
        double x26 = x12*x25;
        double x27 = x18*x25;
        double x28 = 2*ovar;
        double x29 = x26*x28;
        double x30 = x27*x28;
        double x31 = amax*avar;
        double x32 = x31*sin(x11);
        double x33 = x31*sin(x17);
        double x34 = ovar*x22 + ovar*x23 + (-1)*ovar*x16 + (-1)*ovar*x21;
        return x10*(x34 + (-1)*x26*x32) + x10*(x29 + (-1)*x30 + (-1)*x21*x33) + x6*cos(x2) + x9*(x34 + x27*x33) + x9*(x30 + (-1)*x29 + (-1)*x22*x32) + (-1)*x5*((-2)*x16 + (-2)*x21) + (-1)*x5*((-2)*x22 + (-2)*x23) + (-1)*x6*sin(x7) + (-1)*bmax*x10*sin(x2) + (-1)*bmax*x9*cos(x7) + (-1)*x24*x3*(x22 + x23) + (-1)*x24*x4*(x16 + x21) + cmax*x5*cos(cvar*x0)*cos(cvar*x1) + (-8)*x0*x1*x5*(x26 + (-1)*x27);

      }


      //Boundary condition type
      BCType
      bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::DomainType xglobal = is.geometry().global(x);
        if (xglobal[0] + xglobal[1] > 1)
          return Inflow_or_Outflow(is,x);
        else
          return ConvectionDiffusionBoundaryConditions::Dirichlet;
      }

      //! automatic distinction between inflow and outflow boundary condition
      BCType
      Inflow_or_Outflow (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::RangeType evalb = b(is.inside(), is.geometryInInside().global(x));
        typename Traits::RangeFieldType flux = evalb * is.unitOuterNormal(x);
        if (flux > 0) {return ConvectionDiffusionBoundaryConditions::Outflow;}
        return ConvectionDiffusionBoundaryConditions::Neumann;
      }

      //! Dirichlet boundary condition value
      typename Traits::RangeFieldType
      g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        auto x0 = xglobal[0];
        auto x1 = xglobal[1];

        return exp((-1)*(x0*x0 + x1*x1));

      }

      //! Gradient of analytical solution
      typename Traits::RangeType
      solutionGradient (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        typename Traits::RangeType gradient;
        auto x0 = xglobal[0];
        auto x1 = xglobal[1];

        double x2 = 2*exp((-1)*x0*x0 + (-1)*x1*x1);
        gradient[0] = (-1)*x0*x2;
        gradient[1] = (-1)*x1*x2;

        return gradient;
      }

      //! Neumann boundary condition
      typename Traits::RangeFieldType
      j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::DomainType xglobal = is.geometry().global(x);
        auto evalb = b(is.inside(), is.geometryInInside().global(x));
        auto evalA = A(is.inside(), is.geometryInInside().global(x));
        auto evalg = g(is.inside(), is.geometryInInside().global(x));
        auto normal = is.unitOuterNormal(x);
        auto evalgradient = solutionGradient(is.inside(), is.geometryInInside().global(x));
        auto Agradient = evalgradient;
        evalA.mv(evalgradient,Agradient);
        evalb *= evalg;
        return (evalb - Agradient) * normal;
      }

      //! outflow boundary condition
      typename Traits::RangeFieldType
      o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::DomainType xglobal = is.geometry().global(x);
        auto evalA = A(is.inside(), is.geometryInInside().global(x));
        auto normal = is.unitOuterNormal(x);
        auto evalgradient = solutionGradient(is.inside(), is.geometryInInside().global(x));
        auto Agradient = evalgradient;
        evalA.mv(evalgradient,Agradient);
        return -(Agradient * normal);
      }
    };


    //Sample 10: Dimension 3. A=Identity, non-divergence-free b, c=0, Dirichlet boundary conditions. u of infinite order.
    template<typename GV, typename RF>
    class ConvectionDiffusionModelProblemSample10
    {
      typedef ConvectionDiffusionBoundaryConditions::Type BCType;

    public:
      typedef ConvectionDiffusionParameterTraits<GV,RF> Traits;

      //Dimension
      int dimension() {return 3;}

      static bool permeabilityIsConstantPerCell()
      {
        return true;
      }

      //! tensor diffusion coefficient
      typename Traits::PermTensorType
      A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::PermTensorType I;
        typename Traits::DomainType xglobal = e.geometry().global(x);
        for (std::size_t i=0; i<Traits::dimDomain; i++)
        for (std::size_t j=0; j<Traits::dimDomain; j++)
        I[i][j] = (i==j) ? 1 : 0;
        return I;
      }

      //! velocity field
      typename Traits::RangeType
      b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        typename Traits::RangeType v;
        v[0] = cos(13.0*xglobal[0]);
        v[1] = cos(17.0*xglobal[2]);
        v[2] = cos(11.0*xglobal[0]*xglobal[1]);
        return v;
      }

      //! sink term
      typename Traits::RangeFieldType
      c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return cos(37 * xglobal[0]*xglobal[1]*xglobal[2]);
      }

      //! source term
      typename Traits::RangeFieldType
      f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return  exp(xglobal[0] + xglobal[1] + xglobal[2]) * ( -3.0 - 13.0 * sin(13.0*xglobal[0]) + cos(13.0*xglobal[0]) + cos(17.0*xglobal[2]) + cos(11.0*xglobal[0]*xglobal[1]) + cos(37 * xglobal[0]*xglobal[1]*xglobal[2]));
      }


      //Boundary condition type
      BCType
      bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return ConvectionDiffusionBoundaryConditions::Dirichlet;

      }

      //! automatic distinction between inflow and outflow boundary condition
      BCType
      Inflow_or_Outflow (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        typename Traits::RangeType evalb = b(is.inside(), is.geometryInInside().global(x));
        typename Traits::RangeFieldType flux = evalb * is.unitOuterNormal(x);
        if (flux > 0) {return ConvectionDiffusionBoundaryConditions::Outflow;}
        return ConvectionDiffusionBoundaryConditions::Neumann;
      }

      //! Dirichlet boundary condition value
      typename Traits::RangeFieldType
      g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        return exp(xglobal[0] + xglobal[1] + xglobal[2]);

      }

      //! Gradient of analytical solution
      typename Traits::RangeType
      solutionGradient (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        typename Traits::DomainType xglobal = e.geometry().global(x);
        typename Traits::RangeType gradient;

        double x3 = exp(xglobal[0] + xglobal[1] + xglobal[2]);
        gradient[0] = x3;
        gradient[1] = x3;
        gradient[2] = x3;

        return gradient;
      }

      //! Neumann boundary condition
      typename Traits::RangeFieldType
      j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }

      //! outflow boundary condition
      typename Traits::RangeFieldType
      o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }
    };

  }
}


#endif // DUNE_PDELAB_LOCALOPERATOR_CONVECTIONDIFFUSIONPARAMETER_HH

