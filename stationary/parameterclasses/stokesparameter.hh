// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_PDELAB_SYSTEMTESTING_STOKESPARAMETERNEW_HH
#define DUNE_PDELAB_SYSTEMTESTING_STOKESPARAMETERNEW_HH

#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/geometry/type.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/quadraturerules.hh>
#include<dune/pdelab/common/geometrywrapper.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/functionutilities.hh>
#include<dune/pdelab/constraints/common/constraintsparameters.hh>
#include<dune/pdelab/localoperator/stokesparameter.hh>
#include <dune/pdelab/localoperator/dgnavierstokesparameter.hh>
#include<math.h>

namespace Dune {
  namespace PDELab {

    //Problem 0: Constant A,b,c, Dirichlet boundary conditions.
    template<typename GV, typename RF>
    class StokesParameterNew
    {
      typedef StokesBoundaryCondition BCType;

    public:
      typedef NavierStokesParameterTraits<GV,RF> Traits;

      static const bool assemble_navier = true;
      static const bool assemble_full_tensor = false;

      /** \brief Constructor that parses values from parameter tree.

          In order to parse the values correctly
          the ini-file should have the following structure:

          \code
          [parameters]
          rho = 1.0
          mu = 1.0
          \endcode
       */

      StokesParameterNew(const Dune::ParameterTree& parameters)
        : _rho(parameters.get<RF>("rho"))
        , _mu(parameters.get<RF>("mu"))
      {}

      //! Constructor that takes mu and rho directly as arguments
      StokesParameterNew(const RF& mu, const RF& rho)
        : _rho(rho)
        , _mu(mu)
      {}

      //! Dynamic viscosity value from local cell coordinate
      template<typename EG>
      typename Traits::RangeField
      mu(const EG& e, const typename Traits::Domain& x) const
      {
        return _mu;
      }

      //! Dynamic viscosity value from local intersection coordinate
      template<typename IG>
      typename Traits::RangeField
      mu(const IG& ig, const typename Traits::IntersectionDomain& x) const
      {
        return _mu;
      }


      //! Density value from local cell coordinate
      template<typename EG>
      typename Traits::RangeField
      rho(const EG& eg, const typename Traits::Domain& x) const
      {
        return _rho;
      }

      //! Density value from local intersection coordinate
      template<typename IG>
      typename Traits::RangeField
      rho(const IG& ig, const typename Traits::IntersectionDomain& x) const
      {
        return _rho;
      }

      //! source term(vector-valued)
      template<typename EG>
      typename Traits::VelocityRange
      f (const EG& eg, const typename Traits::Domain& x) const
      {
        typename Traits::Domain xglobal = eg.geometry().global(x);
        typename Traits::VelocityRange result(0.0);
        result[0] = 1;
        result[0] -= (std::pow(xglobal[0],2) - 2*std::pow(xglobal[0],3) + std::pow(xglobal[0],4))*(-12 + 24*xglobal[1]);
        result[0] -= (2 - 12*xglobal[0] + 12*std::pow(xglobal[0],2))*(2*xglobal[1] - 6*std::pow(xglobal[1],2) +4*std::pow(xglobal[1],3));

        result[1] = 1;
        result[1] -= (-2*xglobal[0] + 6*std::pow(xglobal[0],2) - 4*std::pow(xglobal[0],3))*(2 - 12*xglobal[1] + 12*std::pow(xglobal[1],2));
        result[1] -= (12 - 24*xglobal[0])*(std::pow(xglobal[1],2) - 2*std::pow(xglobal[1],3) + std::pow(xglobal[1],4));

          if(assemble_navier) {

            result[0] += (2*xglobal[0] - 6*std::pow(xglobal[0],2) + 4*std::pow(xglobal[0],3))*(std::pow(xglobal[0],2) - 2*std::pow(xglobal[0],3) + std::pow(xglobal[0],4))*(2*xglobal[1] - 6*std::pow(xglobal[1],2) + 4*std::pow(xglobal[1],3))*(2*xglobal[1] - 6*std::pow(xglobal[1],2) + 4*std::pow(xglobal[1],3));
            result[0] += (-2*xglobal[0] + 6*std::pow(xglobal[0],2) - 4*std::pow(xglobal[0],3))*(std::pow(xglobal[0],2) - 2*std::pow(xglobal[0],3) + std::pow(xglobal[0],4))*(2 - 12*xglobal[1] + 12*std::pow(xglobal[1],2))*(std::pow(xglobal[1],2) - 2*std::pow(xglobal[1],3) + std::pow(xglobal[1],4));

            result[1] += (-2*xglobal[0] + 6*std::pow(xglobal[0],2) - 4*std::pow(xglobal[0],3))*(-2*xglobal[0] + 6*std::pow(xglobal[0],2) - 4*std::pow(xglobal[0],3))*(2*xglobal[1] - 6*std::pow(xglobal[1],2) + 4*std::pow(xglobal[1],3))*(std::pow(xglobal[1],2) - 2*std::pow(xglobal[1],3) + std::pow(xglobal[1],4));
            result[1] += (-2 + 12*xglobal[0] - 12*std::pow(xglobal[0],2))*(std::pow(xglobal[0],2) - 2*std::pow(xglobal[0],3) + std::pow(xglobal[0],4))*(2*xglobal[1] - 6*std::pow(xglobal[1],2) + 4*std::pow(xglobal[1],3))*(std::pow(xglobal[1],2) - 2*std::pow(xglobal[1],3) + std::pow(xglobal[1],4));
          }

#if 0
        typename Traits::RangeField evalE = E(eg,x);
        f[0] = evalE*(-16.0*xglobal[1] - 8.0*xglobal[0]*xglobal[0]*xglobal[1] - 8.0*xglobal[1]*xglobal[1]*xglobal[1] + evalE*(16.0*xglobal[0]*xglobal[1]*xglobal[1] +4.0*xglobal[0]));
        f[1] = evalE*(16.0*xglobal[0] - 8.0*xglobal[0]*xglobal[1]*xglobal[1] - 8.0*xglobal[0]*xglobal[0]*xglobal[0] + evalE*(16.0*xglobal[0]*xglobal[0]*xglobal[1] +4.0*xglobal[1]));
#endif
        return result;
      }

      //! boundary condition type from local intersection coordinate
      template<typename IG>
      typename Traits::BoundaryCondition::Type
      bctype(const IG& ig,
             const typename Traits::IntersectionDomain& x) const
      {
        typename Traits::Domain xglobal = ig.geometry().global(x);
//         if( xglobal[0] > 1.0-1e-6 )
//           return BCType::DoNothing;
//         else
          return BCType::VelocityDirichlet;
      }

      //! Dirichlet boundary condition value from local cell coordinate
      template<typename EG>
      typename Traits::VelocityRange
      g(const EG& eg, const typename Traits::Domain& x) const
      {
//           typename Traits::Domain xglobal = eg.geometry().global(x);
//           typename Traits::VelocityRange y;
//           y = 0;
//           y[0] = xglobal[1]*(1.0-xglobal[1]);
//           y[0] *= 4.0;
//           return y;

        using std::pow;
        typename Traits::Domain xglobal = eg.geometry().global(x);
        typename Traits::VelocityRange result;
        result[0] = pow(xglobal[0],4) -2*pow(xglobal[0],3) + pow(xglobal[0],2);
        result[0] *= 4*pow(xglobal[1],3) - 6*pow(xglobal[1],2) + 2*xglobal[1];

        result[1] = -(pow(xglobal[1],4) -2*pow(xglobal[1],3) + pow(xglobal[1],2));
        result[1] *= 4*pow(xglobal[0],3) - 6*pow(xglobal[0],2) + 2*xglobal[0];
        return result;



#if 0
        typename Traits::Domain xglobal = eg.geometry().global(x);
        typename Traits::VelocityRange g;
        typename Traits::RangeField evalE = E(eg,x);
        g[0]= 2.0*xglobal[1]*evalE;
        g[1]= 2.0*xglobal[0]*evalE;
        return g;
#endif
      }

      //! pressure source term
      template<typename EG>
      typename Traits::RangeField
      g2(const EG& eg, const typename Traits::Domain& x) const
      {
        return 0;
      }

       //! analytical Pressure (if known, for testing)
      template<typename EG>
      typename Traits::RangeField
      analyticalPressure(const EG& eg, const typename Traits::Domain& x) const
      {
        typename Traits::Domain xglobal = eg.geometry().global(x);
        return xglobal[0] + xglobal[1] - 1;
      }


      //! Neumann boundary condition
      template<typename IG>
      typename Traits::VelocityRange
      j(const IG& ig, const typename Traits::IntersectionDomain& x, const typename Traits::Domain& normal)
      {
        return typename Traits::VelocityRange(0.0);
      }

      private:
        const RF _rho;
        const RF _mu;

      // For concrete problem
      template<typename EG>
      typename Traits::RangeField
      E (const EG& eg, const typename Traits::Domain& x) const
      {
        typename Traits::Domain xglobal = eg.geometry().global(x);
        return exp(xglobal[0]*xglobal[0] + xglobal[1]*xglobal[1]);
      }
    };





    /** \brief Specialization of StokesParameterNew for local operator DGNavierStokes.

        \tparam GV          GridView.
        \tparam RF          The range field type of the Navier-Stokes solution.
    */
    template<typename GV, typename RF>
    class StokesParameterDGNew :
      public StokesParameterNew<GV,RF>
    {
      //! interior penalty type
      typedef Dune::PDELab::DefaultInteriorPenalty<RF> IP;

      //! Typedef of base class
      typedef StokesParameterNew<GV,RF> Base;

    public :

      //! Traits class
      typedef typename Base::Traits Traits;

      /** \brief Constructor that parses values from parameter tree.

          In order to parse the values correctly
          the ini-file should have the following structure:

          \code
          [parameters]
          rho = 1.0
          mu = 1.0
          [parameters.dg]
          epsilon = -1
          sigma = 6.0
          beta = 1.0
          \endcode
       */
      StokesParameterDGNew(const Dune::ParameterTree& parameters)
        : Base(parameters)
        , _ip(parameters.sub("dg"))
        , _epsilon(parameters.sub("dg").get<int>("epsilon"))
      {}


      //! Rescaling factor for the incompressibility equation
      typename Traits::RangeField
      incompressibilityScaling ( typename Traits::RangeField  dt ) const
      {
        typename Traits::RangeField y(1.0 / dt);
        return y;
      }

      /** \brief Get interior penalty parameter from skeleton face.
       */
      template<typename GEO, typename IGEO, typename OGEO>
      typename Traits::RangeField
      getFaceIP(const GEO& geo, const IGEO& igeo, const OGEO& ogeo)
      {
        return _ip.getFaceIP(geo,igeo,ogeo);
      }

      /** \brief Get interior penalty parameter from boundary face.
       */
      template<typename GEO, typename IGEO>
      typename Traits::RangeField
      getFaceIP(const GEO& geo, const IGEO& igeo)
      {
        return _ip.getFaceIP(geo,igeo);
      }

      //! Return the symmetry factor epsilon for this IP
      //! discretization
      int
      epsilonIPSymmetryFactor()
      {
        return _epsilon;
      }

    private:

      IP _ip;       // Interior penalty
      int _epsilon; // IP symmetry factor
    }; // end class DGNavierStokesParameters



  }
}


#endif // DUNE_PDELAB_SYSTEMTESTING_STOKESPARAMETERNEW_HH

