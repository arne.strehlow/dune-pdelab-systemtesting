/********************************************************/
// Beware of line number changes, they may corrupt docu!
//! \brief Driver function to set up and solve the problem
/********************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/io.hh>

#include <dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/subspace.hh>
#include <dune/pdelab/gridfunctionspace/vectorgridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/localoperator/dgnavierstokes.hh>
#include <dune/pdelab/backend/istl.hh>
//#include <dune/pdelab/finiteelementmap/monomfem.hh>
#include <dune/pdelab/finiteelementmap/qkdg.hh>
#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/common/vtkexport.hh>

// #include "navierstokes_initial.hh"
#include "../parameterclasses/stokesparameter.hh"
#include <dune/pdelab/newton/newton.hh>

#define USE_SUPER_LU
#define MAKE_VTK_OUTPUT

template<class GV>
int driver (const GV& gv, Dune::ParameterTree& ptree)
{
  // <<<1>>> important types
  using ES = Dune::PDELab::AllEntitySet<GV>;
  ES es(gv);
  constexpr unsigned int dim = DIMENSION;
  typedef typename GV::Grid::ctype DF; // type for ccordinates
  typedef double RF;                   // type for computations

  //Assemble Parameters
  using StokesProblem = Dune::PDELab::StokesParameterDGNew<GV,RF>;
  StokesProblem problem(ptree.sub("parameters"));

  //start timer
  Dune::Timer watch;
  std::cout << "=== Initialize" << std::endl;
  watch.reset();

  // <<<2>>> Make grid function space
  constexpr int vOrder = DEGREE + 1;
  constexpr int pOrder = DEGREE;

//   typedef Dune::PDELab::MonomLocalFiniteElementMap<DF,RF,dim,vOrder> vFEM;
//   typedef Dune::PDELab::MonomLocalFiniteElementMap<DF,RF,dim,pOrder> pFEM;
//   vFEM vFem(Dune::GeometryType(Dune::GeometryType::cube,dim));
//   pFEM pFem(Dune::GeometryType(Dune::GeometryType::cube,dim));
  typedef Dune::PDELab::QkDGLocalFiniteElementMap<DF,RF,vOrder,dim> vFEM;
  typedef Dune::PDELab::QkDGLocalFiniteElementMap<DF,RF,pOrder,dim> pFEM;
  vFEM vFem;
  pFEM pFem;

  // DOFs per cell
//   constexpr unsigned int vBlockSize = Dune::MonomImp::Size<dim,vOrder>::val;
//   constexpr unsigned int pBlockSize = Dune::MonomImp::Size<dim,pOrder>::val;
  static const unsigned int vBlockSize = vFEM::maxLocalSize();
  static const unsigned int pBlockSize = pFEM::maxLocalSize();

  typedef Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none,vBlockSize> VVectorBackend;
  typedef Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none,pBlockSize> PVectorBackend;
  typedef Dune::PDELab::ISTL::VectorBackend<> VelocityVectorBackend;

#if 1
  // this creates a flat backend (i.e. blocksize == 1)
  typedef Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none> VectorBackend;
#else
  // this creates a backend with static blocks matching the size of the LFS
  typedef Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::fixed> VectorBackend;
#endif

  // velocity
  typedef Dune::PDELab::EntityBlockedOrderingTag VelocityOrderingTag;
  typedef Dune::PDELab::VectorGridFunctionSpace <
    ES, vFEM, dim,
    VelocityVectorBackend,
    VVectorBackend,
    Dune::PDELab::NoConstraints,
    VelocityOrderingTag
    > velocityGFS;
  velocityGFS velocityGfs(es,vFem);
  velocityGfs.name("v");

  // p
  typedef Dune::PDELab::GridFunctionSpace<ES, pFEM, Dune::PDELab::NoConstraints, PVectorBackend> pGFS;
  pGFS pGfs(es,pFem);
  pGfs.name("p");

  // GFS
  typedef Dune::PDELab::EntityBlockedOrderingTag StokesOrderingTag;
  typedef Dune::PDELab::CompositeGridFunctionSpace<VectorBackend, StokesOrderingTag, velocityGFS, pGFS> GFS;
  GFS gfs(velocityGfs, pGfs);
  std::cout << "=== function space setup " <<  watch.elapsed() << " s" << std::endl;

  // <<<3>>> Make coefficient Vector and initialize it from a function
  using V = Dune::PDELab::Backend::Vector<GFS,RF>;
  V x(gfs);


  // Turn parameters from StokesProblem into grid functions
//   auto blambda = [&](const auto& is, const auto& x){return problem.bctype(is,x);};
//   auto b = Dune::PDELab::makeGridFunctionFromCallable(gv,blambda);
//   typedef decltype(b) BType;

/*  auto flambda = [&](const auto& e, const auto& x){return problem.f(e,x);};
  auto f = Dune::PDELab::makeGridFunctionFromCallable(gv,flambda);
  typedef decltype(f) FType;

  typedef BCTypeParamGlobalDirichlet BType;
  BType b;
//  typedef ZeroVectorFunction<ES,RF,dim> FType;
//   FType f(es);
  typedef HagenPoiseuilleVelocityBox<ES,RF,dim> VType;
  VType v(es);
  typedef ZeroScalarFunction<ES,RF> PType;
  PType p(es);
*/
  // <<<4>>> Make grid Function operator
//   watch.reset();
//   typedef Dune::PDELab::DefaultInteriorPenalty<RF> PenaltyTerm;
//
//   typedef Dune::PDELab::DGNavierStokesParameters<ES,RF,FType,BType,VType,PType,false,false,PenaltyTerm>
//     LocalDGOperatorParameters;
//  LocalDGOperatorParameters lop_params(ptree.sub("parameters"),f,b,v,p);


  typedef Dune::PDELab::DGNavierStokes<StokesProblem> LocalDGOperator;
  const int superintegration_order = 0;
  LocalDGOperator lop(problem,superintegration_order);

  typedef Dune::PDELab::EmptyTransformation C;
  typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
  MBE mbe(75); // Maximal number of nonzeroes per row can be cross-checked by patternStatistics().
  typedef Dune::PDELab::GridOperator
    <GFS,GFS,LocalDGOperator,MBE,RF,RF,RF,C,C> GOS;
  GOS gos(gfs,gfs,lop,mbe);

  std::cout << "=== grid operator space setup " <<  watch.elapsed() << " s" << std::endl;

//   typedef typename GOS::Jacobian M;
//   watch.reset();
//   M m(gos);
//   std::cout << m.patternStatistics() << std::endl;
//   std::cout << "=== matrix setup " <<  watch.elapsed() << " s" << std::endl;
//   m = 0.0;
//   watch.reset();
//   gos.jacobian(x,m);
//   std::cout << "=== jacobian assembly " <<  watch.elapsed() << " s" << std::endl;

  // std::ofstream matrix("Matrix");
  // Dune::printmatrix(matrix, m.base(), "M", "r", 6, 3);

//   // evaluate residual w.r.t initial guess
//   V r(gfs);
//   r = 0.0;
//   watch.reset();
//   x = 0.0;
//   gos.residual(x,r);
//   std::cout << "=== residual evaluation " <<  watch.elapsed() << " s" << std::endl;

  bool verbose = true;

//   using Dune::PDELab::Backend::native;
//   typedef Dune::PDELab::Backend::Native<M> ISTLM;
//   typedef Dune::PDELab::Backend::Native<V> ISTLV;
// #ifdef USE_SUPER_LU // use lu decomposition as solver
// #if HAVE_SUPERLU
//   // make ISTL solver
//   Dune::MatrixAdapter<ISTLM,ISTLV,ISTLV> opa(native(m));
//   typedef Dune::SuperLU<ISTLM> LS;
//   LS ls(native(m), verbose?1:0);
//   Dune::InverseOperatorResult stat;
// #else
// #error No superLU support, please install and configure it.
// #endif
// #else // Use iterative solver
//   // make ISTL solver
//   Dune::MatrixAdapter<ISTLM,ISTLV,ISTLV> opa(native(m));
//   Dune::SeqILU0<ISTLM,ISTLV,ISTLV> ilu0(native(m),1.0);
//   typedef Dune::BiCGSTABSolver<ISTLV> LS;
//   LS ls(opa,ilu0,1E-10,20000, verbose?2:1);
//   Dune::InverseOperatorResult stat;
// #endif

//   // solve the jacobian system
//   r *= -1.0; // need -residual
//   x = r;
//   ls.apply(x,r,stat);



  // Linear solver
  typedef Dune::PDELab::ISTLBackend_SEQ_SuperLU LS;
  LS ls(verbose);
  x = 0.0;
  // solve nonlinear problem
  Dune::PDELab::Newton<GOS,LS,V> newton(gos, x, ls);
  newton.setReassembleThreshold(0.0);
  newton.setVerbosityLevel(2);
  newton.setReduction(1e-10);
  newton.setMinLinearReduction(1e-4);
  newton.setMaxIterations(25);
  newton.setLineSearchMaxIterations(30);
  newton.apply();

  //Make discrete grid functions out of vector x
  Dune::PDELab::GridFunctionSubSpace<GFS, Dune::TypeTree::TreePath<0, 0> > gfs00(gfs);
  Dune::PDELab::GridFunctionSubSpace<GFS, Dune::TypeTree::TreePath<0, 1> > gfs01(gfs);
#if DIMENSION == 3
  Dune::PDELab::GridFunctionSubSpace<GFS, Dune::TypeTree::TreePath<0, 2> > gfs02(gfs);
#endif

  typedef Dune::PDELab::GridFunctionSubSpace<GFS, Dune::TypeTree::TreePath<1> > PressureSubGFS;
  PressureSubGFS pressureSubGfs(gfs);
  auto gfs1 = pressureSubGfs;


  // pressure postprocessing
  //--------------------------------------------
  // prepare read from big vector
  typedef Dune::PDELab::LocalFunctionSpace<PressureSubGFS> PressureSubLFS;
  PressureSubLFS pressureSubLfs(pressureSubGfs);
  typedef Dune::PDELab::LFSIndexCache<PressureSubLFS> PressureSubLFSCache;
  PressureSubLFSCache pressureSubLfs_cache(pressureSubLfs);
  typedef typename V::template ConstLocalView<PressureSubLFSCache> ConstPressureSubView;
  ConstPressureSubView constpressureSubview(x);
  std::vector<RF> pressure_local(pressureSubGfs.maxLocalSize());

  // prepare write to big vector
  typedef typename V::template LocalView<PressureSubLFSCache> PressureSubView;
  PressureSubView pressureSubview(x);

  // prepare small sub-vector for pressure
  pGFS pGfs2(gv,pFem);
  using PressureV = Dune::PDELab::Backend::Vector<pGFS,RF>;
  PressureV pressure_z(pGfs2,0.0);
  typedef Dune::PDELab::LocalFunctionSpace<pGFS> pLFS;
  pLFS pLfs(pGfs2);
  typedef Dune::PDELab::LFSIndexCache<pLFS> pLFSCache;
  pLFSCache pLfs_cache(pLfs);
  typedef typename PressureV::template LocalView<pLFSCache> PressureView;
  PressureView p_view;
  p_view.attach(pressure_z);

  // prepare read from small sub-vector for pressure
  typedef typename PressureV::template ConstLocalView<pLFSCache> ConstPressureView;
  ConstPressureView constp_view(pressure_z);

  // constant pressure vector
  PressureV pressure_one(pGfs2,1.0);
  auto pressure_size = pressure_one.flatsize();
  RF pressure_corr;

  // extract pressure coefficient vector
  for(const auto& cell : elements(es))
  {
    // read from vector belonging to composite grid function space
    pressureSubLfs.bind(cell);
    pressureSubLfs_cache.update();
    constpressureSubview.bind(pressureSubLfs_cache);
    constpressureSubview.read(pressure_local);
    constpressureSubview.unbind();

    // std::cout << "======= " << pressure_local[0] << std::endl;

    // write to small vector belonging solely to pressure
    pLfs.bind(cell);
    pLfs_cache.update();
    p_view.bind(pLfs_cache);
    p_view.write(pressure_local);
    p_view.unbind();

    // std::cout << "================ " << native(pressure_z)[16] << std::endl;
  }

  // orthogonalize this small vector to 1-vector
  pressure_corr = pressure_one*pressure_z;
  pressure_corr /= pressure_size;
  pressure_z.axpy(-pressure_corr,pressure_one);

  std::cout << "correction: " << pressure_corr << std::endl;

  // copy back orthogonalized vector
  for(const auto& cell : elements(es))
  {
    // read from small vector belonging solely to pressure
    pLfs.bind(cell);
    pLfs_cache.update();
    constp_view.bind(pLfs_cache);
    constp_view.unbind();
    constp_view.read(pressure_local);

    // write to vector belonging to composite grid function space
    pressureSubLfs.bind(cell);
    pressureSubLfs_cache.update();
    pressureSubview.bind(pressureSubLfs_cache);
    pressureSubview.write(pressure_local);
    pressureSubview.unbind();
  }




  using DiscreteGridFunctionVelocity0 = Dune::PDELab::DiscreteGridFunction<decltype(gfs00), V>;
  DiscreteGridFunctionVelocity0 calculatedVelocity0(gfs00, x);
  using DiscreteGridFunctionVelocity1 = Dune::PDELab::DiscreteGridFunction<decltype(gfs01), V>;
  DiscreteGridFunctionVelocity1 calculatedVelocity1(gfs01, x);
#if DIMENSION == 3
  using DiscreteGridFunctionVelocity2 = Dune::PDELab::DiscreteGridFunction<decltype(gfs02), V>;
  DiscreteGridFunctionVelocity1 calculatedVelocity1(gfs02, x);
#endif
  using DiscreteGridFunctionPressure = Dune::PDELab::DiscreteGridFunction<decltype(gfs1), V>;
  DiscreteGridFunctionPressure calculatedPressure(gfs1, x);

  //Make a composite grid function out of them
//   Dune::PDELab::CompositeGridFunction<DiscreteGridFunctionVelocity0, DiscreteGridFunctionVelocity1> calculatedVelocity(calculatedVelocity0,calculatedVelocity1);
//   Dune::PDELab::CompositeGridFunction<decltype(calculatedVelocity), DiscreteGridFunctionPressure> calculatedSolution(calculatedVelocity0,calculatedPressure);


  //Make grid functions from analytical solution
  auto velocityLambda0 = [&](const auto& e, const auto& x){return problem.g(e,x)[0];};
  auto analyticalVelocity0 = Dune::PDELab::makeGridFunctionFromCallable(gv,velocityLambda0);

  auto velocityLambda1 = [&](const auto& e, const auto& x){return problem.g(e,x)[1];};
  auto analyticalVelocity1 = Dune::PDELab::makeGridFunctionFromCallable(gv,velocityLambda1);

#if DIMENSION == 3
  auto velocityLambda2 = [&](const auto& e, const auto& x){return problem.g(e,x)[2];};
  auto analyticalVelocity2 = Dune::PDELab::makeGridFunctionFromCallable(gv,velocityLambda2);
#endif

  auto pressureLambda = [&](const auto& e, const auto& x){return problem.analyticalPressure(e,x);};
  auto analyticalPressure = Dune::PDELab::makeGridFunctionFromCallable(gv,pressureLambda);
//   Dune::PDELab::CompositeGridFunction<decltype(analyticalVelocity),decltype(analyticalPressure)> solutionAnalytical(analyticalVelocity,analyticalPressure);


  //    #ifdef MAKE_VTK_OUTPUT
  // output grid function with SubsamplingVTKWriter
  Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,2);
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter, gfs, x);
  typedef Dune::PDELab::VTKGridFunctionAdapter<decltype(analyticalVelocity0)> VTKFunctionAdapterVelocity;
  vtkwriter.addVertexData(std::shared_ptr<VTKFunctionAdapterVelocity>(new
                                         VTKFunctionAdapterVelocity(analyticalVelocity0,"exact_v0")));
  auto filename = "dgstokes-2D-2-1";
  vtkwriter.write(filename,Dune::VTK::appendedraw);
  //    #endif



  //Make a grid function out of the pointwise squared error between the calculated and analytical solutions

  auto pointwiseSquaredErrorL2Velocity0 = Dune::PDELab::DifferenceSquaredAdapter<decltype(analyticalVelocity0), decltype(calculatedVelocity0)>(analyticalVelocity0, calculatedVelocity0);
  Dune::FieldVector<double, 1> errorL2Velocity0(0.0);
  Dune::PDELab::integrateGridFunction(pointwiseSquaredErrorL2Velocity0, errorL2Velocity0, 2*DEGREE);
  errorL2Velocity0 = sqrt(errorL2Velocity0);
  std::cout << "L2-error in velocity0 =" << errorL2Velocity0 << std::endl;

  auto pointwiseSquarederrorL2Velocity1 = Dune::PDELab::DifferenceSquaredAdapter<decltype(analyticalVelocity1), decltype(calculatedVelocity1)>(analyticalVelocity1, calculatedVelocity1);
  Dune::FieldVector<double, 1> errorL2Velocity1(0.0);
  Dune::PDELab::integrateGridFunction(pointwiseSquarederrorL2Velocity1, errorL2Velocity1, 2*DEGREE);
  errorL2Velocity1 = sqrt(errorL2Velocity1);
  std::cout << "L2-error in velocity1 =" << errorL2Velocity1 << std::endl;


#if DIMENSION == 3
  auto pointwiseSquarederrorL2Velocity2 = Dune::PDELab::DifferenceSquaredAdapter<decltype(analyticalVelocity2), decltype(calculatedVelocity2)>(analyticalVelocity2, calculatedVelocity2);
  Dune::FieldVector<double, 1> errorL2Velocity2(0.0);
  Dune::PDELab::integrateGridFunction(pointwiseSquarederrorL2Velocity2, errorL2Velocity2, 2*DEGREE);
  errorL2Velocity2 = sqrt(errorL2Velocity2);
  std::cout << "L2-error in velocity2 =" << errorL2Velocity2 << std::endl;
#endif

  auto pointwiseSquarederrorL2Pressure = Dune::PDELab::DifferenceSquaredAdapter<decltype(analyticalPressure), decltype(calculatedPressure)>(analyticalPressure, calculatedPressure);
  Dune::FieldVector<double, 1> errorL2Pressure(0.0);
  Dune::PDELab::integrateGridFunction(pointwiseSquarederrorL2Pressure, errorL2Pressure, 2*DEGREE);
  errorL2Pressure = sqrt(errorL2Pressure);
  std::cout << "L2-error in pressure =" << errorL2Pressure << std::endl;


  double totalErrorL2Velocity = errorL2Velocity0 + errorL2Velocity1;
#if DIMENSION == 3
  totalErrorL2Velocity += errorL2Velocity2;
#endif
  int refinement = ptree.get<int>("grid.refinement");
  std::cout << "Sum of L2-errors in velocity=" << totalErrorL2Velocity << std::endl;
  //std::cout << "H1-error=" << H1error << std::endl;
  std::cout << "Refinement=" << refinement << std::endl;


  //Run test specified in ini file
  auto testtype = ptree.get<std::string>("testtype");

  if (testtype == "L2-test")
  {
    double tolerance = ptree.get<double>("tolerance");
    std::cout << "Running L2-test with tolerance=" << tolerance << std::endl;
    if (totalErrorL2Velocity > tolerance) {
      std::cout << "L2 error in velocity greater than tolerance." << std::endl;
      return 1;
    }
    else if (errorL2Pressure > tolerance) {
      std::cout << "L2 error in pressure greater than tolerance." << std::endl;
      return 1;
    }
    else {
      std::cout << "Test passed" << std::endl;
      return 0;
    }
  }

//   else if (testtype == "H1-test")
//   {
//     double tolerance = ptree.get<double>("tolerance");
//     std::cout << "Running H1-test with tolerance=" << tolerance << std::endl;
//     if (H1error > tolerance) {
//       std::cout << "H1 error greater than tolerance." << std::endl;
//       return 1;
//     }
//     else {
//       std::cout << "Test passed" << std::endl;
//       return 0;
//     }
//   }

  else if (testtype == "L2-convergencetest")
  {
    std::cout << "Running L2-convergencetest" << std::endl;
    RF hmax = pow(2, -refinement);
    //write convergence data in output tree
    Dune::OutputTree outputTree(ptree);
    outputTree.setConvergenceData(totalErrorL2Velocity, hmax, "L2Velocity");
    outputTree.setConvergenceData(errorL2Pressure, hmax, "L2Pressure");
    return 0;
  }

//   else if (testtype == "H1-convergencetest")
//   {
//     std::cout << "Running H1-convergencetest" << std::endl;
//     RF hmax = pow(2, -refinement);
//     //write convergence data in output tree
//     Dune::OutputTree outputTree(ptree);
//     outputTree.setConvergenceData(H1error, hmax);
//     return 0;
//   }

  else
  {
    std::cout << "Unknown testtype. Possible choices are 'L2-test', 'H1-test', 'L2-convergencetest', 'H1-convergencetest'." << std::endl;
  }
  return 0;

}

