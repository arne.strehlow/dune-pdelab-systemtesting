PDELab-Systemtest
=================

This is version 1.0 of Dune-PDELab-Systemtest.

[PDELab-Systemtest][13] provides an automated test for [PDELab][0].

The general idea is to solve a PDE with the tools provided by PDELab and compare
the calculated result to the analytic solution. If the outcome is "correct", it is highly 
likely that all components used in the program are working as intended.

More precisely, we run our code with two regular meshes (one coarser, one finer) 
and look at the errors (say, in L2-norm). If the error behaves as expected (with
respect to h, the size of the mesh elements), the test is passed.

Currently the testing focuses on commonly used local operators in a stationary, 
linear problem.


Dependencies
------------

PDELab depends on the following software packages:

* DUNE-PDELab

* DUNE-Testtools


For a full explanation of the DUNE installation process please read
the [installation notes][2].


Tested local operators
----------------------

* ConvectionDiffusionFEM
* ConvectionDiffusionCCFV
* ConvectionDiffusionDG
* DGNavierStokes
* TaylorHoodNavierStokes


Tested function spaces
----------------------

* QkLocalFiniteElementMap (degree 0,1,2)
* PkLocalFiniteElementMap (degree 1,2,3,4)
* QkDGLocalFiniteElementMap (degree 1,2,3,4)

License
-------

Library, headers and test programs are free open-source software,
dual-licensed under version 3 or later of the GNU Lesser General Public License
and version 2 of the GNU General Public License with a special run-time exception.

See the file [LICENSE.md][12] for full copying permissions.

Installation
------------

For installation instructions please see the [DUNE website][1].

Links
-----

 [0]: http://www.dune-project.org/pdelab/
 [1]: http://www.dune-project.org
 [2]: http://www.dune-project.org/doc/installation-notes.html
 [4]: http://gcc.gnu.org/onlinedocs/libstdc++/faq.html#faq.license
 [5]: http://www.mcs.anl.gov/petsc/
 [6]: http://eigen.tuxfamily.org
 [8]: http://lists.dune-project.org/mailman/listinfo/dune-pdelab
 [9]: http://gitlab.dune-project.org/pdelab/dune-pdelab/issues
[10]: http://gitlab.dune-project.org/pdelab/dune-typetree
[11]: CHANGELOG.md
[12]: LICENSE.md
[13]: https://gitlab.dune-project.org/arne.strehlow/dune-pdelab-systemtesting/
